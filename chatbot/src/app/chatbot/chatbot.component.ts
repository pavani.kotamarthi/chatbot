import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css'],
})
export class ChatbotComponent implements OnInit {
  @ViewChild('chatBoxPopup', { static: true })
  chatBoxPopup!: ElementRef;
  diseases = [
    'Coma',
    'Back Pain',
    'Runny Nose',
    'Belly Pain',
    'Acidity',
    'Common Cold',
    'None',
  ];
  stethoscope = '/assets/icons/stethoscope.svg';
  constructor() {}
  ngOnInit() {}
  viewChatBox() {
    this.chatBoxPopup.nativeElement.classList.add('active');
  }
}
